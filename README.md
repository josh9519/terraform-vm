# Vagrant 1
First vagrant session using .yml files and functions in the Vagrantfile.
## guest_machines.yml
```
name:
box:
cpus:
memory:
package_manager:
packages:
scripts:
forwarded_ports:
- guest:
  host:
```
**name** - name of the machine.

**box** - the OS to be used using the name given by the name of the vagrant box

**cpus** - number of CPUs to be made

**memory** - amount of memory required

**package_manager** - name of the corresponding package manager of the OS

**packages** - array of packages which need to be installed on the machine

**scripts** - scripts to be run when the VM is made

**forwarded_ports:** - array of ports with **guest** and **host** ports defined to define the port forwarding rule

The last 4 items are optional.
## Vagrantfile
The first 2 lines are required to tell the script that we will be using a yaml file and load the file as the object **guests**

For each item in the array **guests** (all the information for each VM), this code goes through and defines all of the things required using functions.

